from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=User)
def register_profile(sender, **kwargs):
    Profile.objects.create(user=kwargs['instance'])

class Profile(models.Model):
    user = models.OneToOneField(User)
    questions_number = models.IntegerField(default=0)
    answers_number = models.IntegerField(default=0)

    def __unicode__(self):
        return self.user.username


